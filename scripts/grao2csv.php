<?php

$grao_str = file_get_contents('http://www.grao.bg/tna/tab02.txt');
$grao_str = iconv("CP1251", "UTF-8", $grao_str);
$grao = explode("\r\n", $grao_str);

$csv = "Област,Община,Г/С,Име,Постоянен адрес,Настоящ адрес,Постоянен и настоящ\n";
foreach ($grao as $grao_line) {
	if (preg_match("/област\s(.+)\sобщина\s(.+)/", $grao_line, $m)):
		$obl = mb_convert_case(trim($m[1]), MB_CASE_TITLE, "UTF-8");
		$obs = mb_convert_case(trim($m[2]), MB_CASE_TITLE, "UTF-8");
	elseif (preg_match("/\|ГР.(.+)[\s+]?\|\s+(.+)\|\s+(.+)\|\s+(.+)\|/", $grao_line, $m)):
		if (!strstr($m[1], ',')):
			$csv .= "$obl,$obs,Г,".mb_convert_case(trim($m[1]), MB_CASE_TITLE, "UTF-8").",$m[2],$m[3],$m[4]\n";
		endif;
	elseif (preg_match("/\|С.(.+)[\s+]?\|\s+(.+)\|\s+(.+)\|\s+(.+)\|/", $grao_line, $m)):
		if (!strstr($m[1], ',')):
			$csv .= "$obl,$obs,С,".mb_convert_case(trim($m[1]), MB_CASE_TITLE, "UTF-8").",$m[2],$m[3],$m[4]\n";
		endif;
	endif;
}

$handle = fopen('grao.csv', 'w');
fwrite($handle, $csv);
fclose($handle);

?>

