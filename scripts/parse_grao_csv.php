<?php 
/*
 * Таблицата settlements_full вече съдържа данните от 
 * https://github.com/yurukov/Bulgaria-geocoding/blob/master/settlements.csv
 *
 */
$file = "grao.csv";

$username="username";
$password="password";
$database="database";
$server="127.0.0.1";

$link = new mysqli($server, $username, $password, $database);

if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}
if (!mysqli_set_charset($link, "utf8")) 
    printf("Error loading character set utf8: %s\n", mysqli_error($link));


$data=fopen($file,'r');
while($row=fgets($data)){
	$elements = explode(",", $row);
	
	$province = $elements[0];
	$municipality = $elements[1];
	if ($elements[2] == "С")
	    $is_village = 1;
    else
        $is_village = 0;
    $name = $elements[3];
    $permanent = $elements[4];
    $current = $elements[5];
    $permanent_and_current = $elements[6];
    
    // Fix names
    if ($name == "Бов (Гара Бов)")
        $name = "гара Бов";
    if ($name == "Лакатник(Гара Лакатник)")
        $name = "гара Лакатник";
        
    // Fix provices
    if ($province == "София")
        $province = "София (столица)";
    if ($province == "Софийска")
        $province = "София";
    
    // Fix municipality names
    if ($municipality == "Добричка")
        $municipality = "Добрич-селска";
    if ($municipality == "Добрич-Град")
        $municipality = "Добрич";
    
  
    $sql = "UPDATE settlements_full SET `permanent`=".$permanent.", `current`=".$current.", `permanent_and_current`=".$permanent_and_current." WHERE `name` = '".$name."' AND `municipality` = '".$municipality."' AND `province` = '".$province."'";

	//echo $sql."\n";
	if (!mysqli_query($link, $sql))
		print('Error: ' . mysqli_error($link));
}

echo "Done !\n";

